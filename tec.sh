#!/bin/bash
#############################################
#       Function encrypt and delete a target
#
#       Author: motan (bioshark.dev@gmail.com)
#
#       Parameters
#       1) recipient - key to use for encyption
#       2) target - folder/file name with path
#############################################

# test if given folder is a folder
usage() {
        echo "tec (tar, encrypt, clean) 0.1"
        echo "Author: motan (bioshark.dev@gmail.com)"
        echo "Tarballs and encrypts the TARGET using gpg (GnuPG) encrypton. Optionally it deletes the TARGET"
        echo
        echo "Usage: dirtest [OPTION]... TARGET"
        echo
        echo "Options:"
        echo                                    
        echo " -d,                      delete the original target"
        echo " -r, NAME                 encrypt for NAME"
        echo " -h,                      diplay this help"
        echo
}

version() {
        echo "tec (tar, encrypt, clean) 0.1"
        echo "Author: motan (bioshark.dev@gmail.com)"
        echo "Tarballs and encrypts the TARGET using gpg (GnuPG) encrypton. Optionally it deletes the TARGET"
}

encrypt()
{
	gpg --encrypt --recipient $1 -v $2
}

########## BEGIN #################

# test if help is neede
if test $# = 1; then
case "$1" in
        --help | --hel | --he | --h )
        usage; exit 0 ;;
        --version | --versio | --versi | --vers | --ver | --ve | --v )
        version; exit 0 ;;
esac
fi

while getopts ":dr:h" opt; do
        case $opt in
                d ) delorig="TRUE" ;;
                r ) recipient=$OPTARG ;;
                h ) usage; exit 0  ;;
                \? ) usage; exit 0
        esac

done

# shifting based on the number of options, 
#       so that the next one is the path
shift $(($OPTIND - 1))
target=$1

# determine the target's name
if [ -d $target ] 
then
        # - remove the last / char if needed, 
        # - pad it, to pattern /something/name
        # - extract name
        trimend="/dummy"${target%/}
        targetName=${trimend##/*/}
else
        targetName=${target%.*}
fi

# tarball the target
echo "Making tarball $targetName.tar.gz from:"
tar cvzf $targetName.tar.gz $target
echo

# encrypt the target
echo "Encrypting $target"
encrypt $recipient $target
echo

# delete the target if required
if [ ${delorig:-"FALSE"} = "TRUE" ] 
then
        echo "Remove target, keep only encrypted files"
        rm -rv $target $targetName.tar.gz
fi

